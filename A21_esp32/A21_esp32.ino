#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <ArduinoJson.h>
#include <FreeRTOS.h>

#define DEVICE_NAME   "ESP32Ethanol"



//#define WIFI
#define BLUETOOTH

#if defined (WIFI) & defined (BLUETOOTH)
#error "Wifi et Bluetooth"
#endif

#ifdef WIFI

const char *SSID = DEVICE_NAME;
const char *PWD = "123456789";
 
// Web server running on port 80
WebServer server(80);
 

// JSON data buffer
StaticJsonDocument<250> jsonDocument;
char buffer[250];
 
// env variable
float temperature;
uint8_t ratio;
 
void PrepareWiFi_AP() 
{
  WiFi.softAP(SSID, PWD);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
}

void setup_routing() 
{     
  server.on("/temperature", getTemperature);     
  server.on("/ratio", getRatio);     
     
  server.on("/postratio", HTTP_POST, handlePost);    
       
  // start server    
  server.begin();    
}
 
//void create_json(char *tag, float value, char *unit) 
//{  
//  jsonDocument.clear();  
//  jsonDocument["type"] = tag;
//  jsonDocument["value"] = value;
//  jsonDocument["unit"] = unit;
//  serializeJson(jsonDocument, buffer);
//}
// 
//void add_json_object(char *tag, float value, char *unit) 
//{
//  JsonObject obj = jsonDocument.createNestedObject();
//  obj["type"] = tag;
//  obj["value"] = value;
//  obj["unit"] = unit; 
//}

 
void getTemperature() 
{
  Serial.println("Get temperature");

  //Serial2.write(rxSerialBT);
  temperature = 99;
  
  jsonDocument.clear();  
  jsonDocument["temperature"] = temperature;
  serializeJson(jsonDocument, buffer);
  
  server.send(200, "application/json", buffer);
}
 
void getRatio() 
{
  Serial.println("Get Ratio");
  ratio = 50;
  
  jsonDocument.clear();  
  jsonDocument["ratio"] = ratio;
  serializeJson(jsonDocument, buffer);

  delay(5000);
  server.send(200, "application/json", buffer);
}

void handlePost() 
{
  if (server.hasArg("plain") == false) 
  {
    //handle error here
  }
  
  String body = server.arg("plain");
  deserializeJson(jsonDocument, body);
  
  // Get RGB components
  int POSTratio = jsonDocument["ratio"];
  Serial.print("ratio: ");
  Serial.print(POSTratio);

  // Respond to the client
  server.send(200, "application/json", "{\"message\":\"OK\"}");
}

//void setup_task() 
//{    
//  xTaskCreate(     
//  read_sensor_data,      
//  "Read sensor data",      
//  1000,      
//  NULL,      
//  1,     
//  NULL     
//  );     
//}

void setup() {     
  Serial.begin(115200);   

  Serial2.begin(9600);  //Vers STM32
     
  PrepareWiFi_AP();     
 // setup_task();    
  setup_routing();     
}    
       
void loop() 
{    
  server.handleClient();     
}


#else

//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Evandro Copercini - 2018
//
//This example creates a bridge between Serial and Classical Bluetooth (SPP)
//and also demonstrate that SerialBT have the same functionalities of a normal Serial

#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
int rxSerial2, rxSerialBT;

void setup() 
{
  Serial.begin(115200); //console
  SerialBT.begin(DEVICE_NAME); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

  Serial2.begin(9600);  //Vers STM32
}

void loop() 
{
  
  
  if (Serial2.available()) 
  {
    rxSerial2 = Serial2.read();
    SerialBT.write(rxSerial2);

    Serial.write(rxSerial2);  //console
  }
  
  if (SerialBT.available()) 
  {
    rxSerialBT = SerialBT.read();
    Serial2.write(rxSerialBT);

    Serial.write(rxSerialBT);  //console
  }
  delay(20);
}
#endif
